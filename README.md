# README #

Passos para execução do trabalho de Sistemas Distribuidos - UECE. 

### Clonar o repositório ###

Em um software ou terminal com git, clone o repositório através do seguinte comando:git clone https://marcosborges1@bitbucket.org/marcosborges1/trabalhosd.git

### Passos para a execução ###
No mininet, vá até o diretório "src" e execute o script mininet_script.py

Obs:
Compile os arquivos .java dos diretórios rsa e socket para a correta execução do script mininet_script.py

Atenciosamente, Marcos Borges.