#!/usr/bin/python

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.util import dumpNodeConnections
from mininet.log import setLogLevel
import time

serverPrefix = 'primary'
switchPrefix = 'switch'
hostPrefix = 'host'
delimiter = '_'
primaryHostIp = '10.1.0.1'
N = 4

class Topologia(Topo):

    def build(self, n=2):
        switch = self.addSwitch('%s%s%s' % (switchPrefix, delimiter, 1))
        primary = self.addHost('%s%s%s' % (serverPrefix, delimiter, 1), ip=primaryHostIp)
        
        self.addLink(primary, switch)

        for h in range(n):
            host = self.addHost('%s%s%s' % (hostPrefix, delimiter, h+1))
            self.addLink(host, switch)

def main():

    time.sleep(1)

    topo = Topologia(n=N)
    net = Mininet(topo)
    net.start()

    hostname = '%s%s%d' % (serverPrefix, delimiter, 1)
    primary = net.get(hostname)

    print primary.cmd("java socket.ServidorTCP %s %s &" % (hostname, primaryHostIp))

    for i in range(N):
        hostname = '%s%s%d' % (hostPrefix, delimiter, i+1)
        host = net.get(hostname)
        print '%s: %s' % (hostname, host.IP())
        print host.cmd("java socket.ClienteTCP %s %s" % (hostname, primaryHostIp))

    for i in range(N):
        hostname = '%s%s%d' % (hostPrefix, delimiter, i+1)
        host = net.get(hostname)
        print host.cmd("sudo killall java")
    
    print primary.cmd('sudo killall java')
    net.stop()

if __name__ == '__main__':
    setLogLevel('info')
    #setLogLevel('debug')
    #setLogLevel('output')
    main()