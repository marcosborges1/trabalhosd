package rsa;

import java.util.Random;

public final class NumerosPrimos {

	public static int gerarNumeroPrimoAleatorioNoIntervaloDe(int intervalo) {

		Random gerador = new Random();
		boolean naoForPrimo = true;
		int numeroPrimoAleatorio = 0;
		while (naoForPrimo) {
			int numeroAleatorio = gerador.nextInt(intervalo);
			if (isPrime(numeroAleatorio) && numeroAleatorio > 2) {
				numeroPrimoAleatorio = numeroAleatorio;
				naoForPrimo = false;
			}
		}
		return numeroPrimoAleatorio;

	}

	public static boolean isPrime(long number) {
		number = Math.abs(number);
		if (number % 2 == 0) {
			return false;
		}
		for (long i = 3; i * i <= number; i += 2) {
			if (number % i == 0) {
				return false;
			}
		}
		return true;
	}

	public static int proximoNumeroPrimo(int numero) {
		boolean naoForPrimo = true;
		int novoNumeroPrimo = ++numero; // Mais um para nao ser igual ao número primo
		while (naoForPrimo) {
			if (isPrime(novoNumeroPrimo)) {
				naoForPrimo = false;
			} else {
				novoNumeroPrimo++;
			}
		}
		return novoNumeroPrimo;
	}
}
