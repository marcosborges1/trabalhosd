package rsa;

import java.io.UnsupportedEncodingException;

public class Aplicacao {

	public static void main(String[] args) throws UnsupportedEncodingException {
		
		Host hostA = new Host("Host A");
		GeradorDeChaves geradorDeChavesDoHostA = new GeradorDeChaves(17,11,7);
		hostA.setChavePublica(geradorDeChavesDoHostA.getChavePublica());
		hostA.setChavePrivada(geradorDeChavesDoHostA.getChavePrivada());
		
		System.out.println(hostA);
		
		Host hostB = new Host("Host B");
		GeradorDeChaves geradorDeChavesDoHostB = new GeradorDeChaves(29,37,1079);
		hostB.setChavePublica(geradorDeChavesDoHostB.getChavePublica());
		hostB.setChavePrivada(geradorDeChavesDoHostB.getChavePrivada());
		
		System.out.println(hostB);
		
		//String mensagemCripografadaComChaveDeB = Criptografia.criptografarMensagem("4564", hostB.getChavePublica());
		
		hostA.enviarMensagemCriptografada(Criptografia.criptografarMensagem("Ei", hostB.getChavePublica()), hostB);
		hostA.enviarMensagemCriptografada(Criptografia.criptografarMensagem("doio", hostB.getChavePublica()), hostB);
		hostA.enviarMensagemCriptografada(Criptografia.criptografarMensagem("Massa", hostB.getChavePublica()), hostB);
		
		hostB.lerMensagens();
		
		hostB.enviarMensagemCriptografada(Criptografia.criptografarMensagem("Fala ai maluco", hostA.getChavePublica()), hostA);
		
		hostA.lerMensagens();
//		String mensagemCripografada = Criptografia.criptografarMensagem("4564", hostB.getChavePublica());
//		System.err.println(mensagemCripografada);
//		
//		String mensagemDescriptografada = Criptografia.desCriptografarMensagem(mensagemCripografada, hostB.getChavePrivada());
//		System.err.println(mensagemDescriptografada);
		
		//GeradorDeChaves geradorDeChaves = new GeradorDeChaves();
//		GeradorDeChaves geradorDeChaves = new GeradorDeChaves(17,11,7);
//		GeradorDeChaves geradorDeChaves = new GeradorDeChaves(29,37,1079);
//		
//		int[] chavePrivada = geradorDeChaves.getChavePrivada();
//		int[] chavePublica = geradorDeChaves.getChavePublica();
//		
//		String mensagemCripografada = Criptografia.criptografarMensagem("4564", chavePublica);
//		
//		String mensagemDescriptografada = Criptografia.desCriptografarMensagem(mensagemCripografada, chavePrivada);
//		
//		System.err.println(mensagemCripografada);
//		System.err.println(mensagemDescriptografada);
		
	}

}
