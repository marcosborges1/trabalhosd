package rsa;

public class GeradorDeChaves {

	private int p, q, d, n, z, e;

	public GeradorDeChaves() {
		setP(NumerosPrimos.gerarNumeroPrimoAleatorioNoIntervaloDe(100));
		setQ(NumerosPrimos.gerarNumeroPrimoAleatorioNoIntervaloDe(30));
		calcularN();
		calcularZ();
		calcularD();
		calcularE();
	}

	public GeradorDeChaves(int p, int q, int d) {
		setP(p);
		setQ(q);
		calcularN();
		calcularZ();
		setD(d);
		calcularE();
	}

	public void calcularN() {
		setN(getP() * getQ());
	}

	public void calcularZ() {
		setZ((getP() - 1) * (getQ() - 1));
	}

	public void calcularD() {
		setD(NumerosPrimos.proximoNumeroPrimo(getZ()));
	}

	public void calcularE() {
		boolean naoObterE = true;
		int e = 1;
		while (naoObterE) {
			int calculoDeE = (e * getD()) % getZ();
			if (calculoDeE == 1) {
				naoObterE = false;
				setE(e);
			} else {
				e++;
			}
		}
	}

	public int[] getChavePublica() {
		int[] chavePublica = { getN(), getE() };
		return chavePublica;
	}

	public int[] getChavePrivada() {
		int[] chavePublica = { getN(), getD() };
		return chavePublica;
	}

	public String getChavePorExtenso(int[] chave) {
		return "(" + chave[0] + "," + chave[1] + ")";
	}

	public int[] getChaves(String chavesPorExtenso) {
		String chavesSemParentes = chavesPorExtenso.replace("(", "").replace(")", "");
		String[] chaveSeparada = chavesSemParentes.split(",");
		int[] vetorChaves = new int[2];
		for (int i = 0; i < chaveSeparada.length; i++) {
			vetorChaves[i] = Integer.parseInt(chaveSeparada[i]);
		}
		return vetorChaves;
	}

	/* Getters and Setters */
	public int getP() {
		return p;
	}

	public void setP(int p) {
		this.p = p;
	}

	public int getQ() {
		return q;
	}

	public void setQ(int q) {
		this.q = q;
	}

	public int getD() {
		return d;
	}

	public void setD(int d) {
		this.d = d;
	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}

	public int getZ() {
		return z;
	}

	public void setZ(int z) {
		this.z = z;
	}

	public int getE() {
		return e;
	}

	public void setE(int e) {
		this.e = e;
	}

}
