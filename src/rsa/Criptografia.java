package rsa;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;

public class Criptografia {

	public static String criptografarMensagem(String mensagemOrignal, int[] chavePublica)
			throws UnsupportedEncodingException {
		String mensagemPreCodificada = prepararTextoParaCodificacao(mensagemOrignal.getBytes(StandardCharsets.US_ASCII),
				3);
		String mensagemCriptografada = submeterCodificacao(mensagemPreCodificada, chavePublica);
		return mensagemCriptografada;
	}

	private static String submeterCodificacao(String mensagemPreCodificada, int[] chavePublica) {

		String mensagemCriptografada = "";
		String arryaNovasMensagens[] = quebrarMensagemEmTamanhosIguais(mensagemPreCodificada, 3);
		for (String mensagemQuebrada : arryaNovasMensagens) {
			mensagemCriptografada += criptografar(mensagemQuebrada, chavePublica);
		}
		return mensagemCriptografada;
	}

	public static String desCriptografarMensagem(String mensagemCripotagrafada, int[] chavePrivada)
			throws UnsupportedEncodingException {

		String mensagemDecodificada = submeterDecodificacao(mensagemCripotagrafada, chavePrivada);
		return mensagemDecodificada;
	}

	private static String submeterDecodificacao(String mensagemCriptografada, int[] chavePrivada) {

		String mensagemDescriptografada = "";

		String arryaNovasMensagens[] = quebrarMensagemEmTamanhosIguais(mensagemCriptografada, 3);
		for (String mensagemQuebrada : arryaNovasMensagens) {
			mensagemDescriptografada += descriptografar(mensagemQuebrada, chavePrivada);
		}
		return mensagemDescriptografada;
	}

	public static String criptografar(String pedacoMensagem, int[] chavePublica) {

		BigInteger textoCriptografado = new BigInteger(pedacoMensagem).pow(chavePublica[1])
				.mod(new BigInteger("" + chavePublica[0]));
		return completarStringComZeros(textoCriptografado.toString(), 3);

	}

	public static char descriptografar(String textoCriptografado, int[] chavePrivada) {
		BigInteger textoDescriprografado = new BigInteger(textoCriptografado).pow(chavePrivada[1])
				.mod(new BigInteger("" + chavePrivada[0]));
		return (char) textoDescriprografado.intValue();
	}

	public static String prepararTextoParaCodificacao(byte[] arrayDeBytes, int divisaoPorBloco)
			throws UnsupportedEncodingException {

		String novaStringCodificada = "";
		for (byte byteEmQuestao : arrayDeBytes) {
			novaStringCodificada += completarStringComZeros("" + byteEmQuestao, divisaoPorBloco);
		}
		return novaStringCodificada;
	}

	public static String completarStringComZeros(String name, int tamanho) {
		int quantidadeDeZerosQueFaltam = tamanho - name.length();
		String novaString = name;
		for (int j = 0; j < quantidadeDeZerosQueFaltam; j++) {
			novaString = "0" + novaString;
		}
		return novaString;
	}

	public static String[] quebrarMensagemEmTamanhosIguais(String mensagem, int tamanho) {
		int tamanhoTotalMensagem = mensagem.length();
		String[] vetor = new String[tamanhoTotalMensagem / tamanho];
		int j;
		for (int i = 0; i < tamanhoTotalMensagem / tamanho; i++) {
			j = i * tamanho;
			vetor[i] = mensagem.substring(j, j + 3);
		}
		return vetor;
	}

}
